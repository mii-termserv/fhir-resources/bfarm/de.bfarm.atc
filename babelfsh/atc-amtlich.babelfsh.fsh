Alias: $sutermserv_project = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-project
Alias: $sutermserv_dataset = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-dataset
Alias: $sutermserv_license = https://mii-termserv.de/fhir/su-termserv/CodeSystem/mii-cs-suts-resource-tags-license

RuleSet: bfarm-sutermserv-tags
* ^meta.tag[+].system = "$sutermserv_project"
* ^meta.tag[=].code = #bfarm
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_dataset"
* ^meta.tag[=].code = #bfarm
* ^meta.tag[=].display = "Bundesinstitut für Arzneimittel und Medizinprodukte"
* ^meta.tag[+].system = "$sutermserv_license"
* ^meta.tag[=].code = #https://mii-termserv.de/licenses#bfarm
* ^meta.tag[=].display = "BfArM license terms"

RuleSet: atc-babelfsh(version, input_file, sheet)
* ^url = "http://fhir.de/CodeSystem/bfarm/atc"
* insert bfarm-sutermserv-tags
* ^meta.profile = "http://hl7.org/fhir/StructureDefinition/shareablecodesystem"
* ^extension[+].url = "http://hl7.org/fhir/StructureDefinition/cqf-scope"
* ^extension[=].valueString = "@mii-termserv/de.bfarm.atc"
* ^version = "{version}"
* ^status = #active
* ^experimental = false
* ^caseSensitive = false
* ^content = #complete
* ^publisher = "Wissenschaftliches Institut der AOK (WIdO)"
* ^copyright = "Die Erstellung erfolgt unter Verwendung der maschinenlesbaren Fassung des Wissenschaftlichen Institutes der AOK (WiDO)."
* ^identifier[+].system = "urn:ietf:rfc:3986"
* ^identifier[=].value = "http://fhir.de/CodeSystem/dimdi/atc"
* ^identifier[=].use = #old
* ^property[+].code = "ddd"
* ^property[=].description = "Die definierte Tagesdosis (defined daily dose, DDD) ist ein Maß für die verordnete Arzneimittelmenge. Die DDD basiert auf der Menge eines Wirkstoffs, die typischerweise in der Hauptindikation bei Erwachsenen pro Tag angewendet wird."
* ^property[=].type = "string"
* ^property[=].uri = "https://www.wido.de/publikationen-produkte/analytik/arzneimittel-klassifikation"
/*^babelfsh
atc-ddd --file={input_file} --sheet="{sheet}" --ddd-property-column=4 --ddd-property-code="ddd"
^babelfsh*/

CodeSystem: AtcAmtlich
Id: atc-amtlich-2025
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2025", "./input-files/atc-2025-amtlich.xlsm", "amtl. Index 2025 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2024
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2024", "./input-files/atc-2024-amtlich.xlsm", "amtl.Index 2024 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2023
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2023", "./input-files/atc-2023-amtlich.xlsm", "Amtl.Index 2023 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2022
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2022", "./input-files/atc-2022-amtlich.xlsm", "Amtl. Index 2022 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2021
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2021", "./input-files/atc-2021-amtlich.xlsm", "Amtl. Index 2021 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2020
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2020", "./input-files/atc-2020-amtlich.xlsm", "Amtl. Index 2020 ATC-sortiert")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2019
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2019", "./input-files/atc-2019-amtlich.xlsm", "amtlicher Index 2019")

CodeSystem: AtcAmtlich
Id: atc-amtlich-2018
Title: "Anatomisch-therapeutisch chemische Klassifikation (ATC) Amtliche deutsche Fassung"
Description: "Anatomisch-therapeutisch-chemische-Klassifikation mit Tagesdosen Amtliche Fassung des ATC-Index mit DDD-Angaben für Deutschland"
* insert atc-babelfsh("2018", "./input-files/atc-2018-amtlich.xlsx", "amtlicher-Index sortiert_2018")